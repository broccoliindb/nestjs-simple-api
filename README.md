## Nestjs playground

Nestjs를 통한 playground projects

nodejs framework

- nodejs를 좀 더 업격한 틀위에서 구현하도록 해줌.

controller: nodejs처럼 라우트 역할을 한다. url을 가지고 오고 함수를 리턴한다.
service: 실제 비즈니스 로직을 정의한 부분. 필요하다면 db에 연결한다.
module: 하나의 로직을 패키징한 부분.