import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMovieDto } from './dto/create-moive.dto';
import { UpdateMovieDto } from './dto/update-moive.dto';
import { Movie } from './entities/movie.entity';

@Injectable()
export class MoviesService {
  private movies: Movie[] = [];

  getAll(): Movie[] {
    return this.movies;
  }

  getOne(id: number): Movie {
    const movie = this.movies.find((movie) => movie.id === id);
    if (!movie) {
      throw new NotFoundException(`Movie with Id ${id} is not found`);
    }
    return movie;
  }

  deleteOne(id: number) {
    this.getOne(id);
    this.movies = this.movies.filter((movie) => movie.id !== +id);
  }

  createMovie(movieData: CreateMovieDto): Movie {
    const movie = {
      id: Date.now(),
      ...movieData,
    };
    this.movies.push(movie);
    return movie;
  }

  update(id: number, updateData: UpdateMovieDto): Movie {
    const movie = this.getOne(id);
    this.deleteOne(id);
    const updatedMovie = { ...movie, ...updateData };
    this.movies.push(updatedMovie);
    return updatedMovie;
  }
}
