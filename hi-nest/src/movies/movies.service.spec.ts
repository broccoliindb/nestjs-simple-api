import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { MoviesService } from './movies.service';

describe('MoviesService', () => {
  let service: MoviesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MoviesService],
    }).compile();

    service = module.get<MoviesService>(MoviesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAll', () => {
    it('should return an array', () => {
      const result = service.getAll();

      expect(result).toBeInstanceOf(Array);
    });
  });

  describe('getOne', () => {
    it('should return a movie', () => {
      const movie = service.createMovie({
        title: 'test moive',
        genres: ['test'],
        year: 2000,
      });
      const m = service.getOne(movie.id);
      expect(m).toBeDefined();
      expect(m.id).toEqual(movie.id);
    });

    it('should throw 404 error', () => {
      try {
        service.getOne(111);
      } catch (e) {
        expect(e).toBeInstanceOf(NotFoundException);
        expect(e.message).toEqual('Movie with Id 111 is not found');
      }
    });
  });

  describe('deleteOne', () => {
    it('delete a movie', () => {
      const movie = service.createMovie({
        title: 'test moive',
        genres: ['test'],
        year: 2000,
      });
      const allMovies = service.getAll();
      service.deleteOne(movie.id);
      const afterDelete = service.getAll();
      expect(afterDelete.length).toBeLessThan(allMovies.length);
      try {
        service.deleteOne(movie.id);
      } catch (e) {
        expect(e).toBeInstanceOf(NotFoundException);
      }
    });
  });

  describe('createMovie', () => {
    it('should create a movie', () => {
      const beforeMoviesLength = service.getAll().length;
      const movie = service.createMovie({
        title: 'test moive',
        genres: ['test'],
        year: 2000,
      });
      const allMoviesLength = service.getAll().length;
      const allMovies = service.getAll();
      expect(beforeMoviesLength).toBeLessThan(allMoviesLength);
      expect(allMovies.find((i) => i.id === movie.id)).toBeDefined();
    });
  });

  describe('update', () => {
    it('should update a movie', () => {
      const movie = service.createMovie({
        title: 'test moive',
        genres: ['test'],
        year: 2000,
      });
      const updatedMovie = service.update(movie.id, { year: 2020 });
      const allMovies = service.getAll();
      expect(
        allMovies.find(
          (i) => i.id === updatedMovie.id && i.year === updatedMovie.year,
        ),
      ).toBeDefined();
    });
    it('throw 404', () => {
      try {
        service.update(999, { year: 2020 });
      } catch (e) {
        expect(e).toBeInstanceOf(NotFoundException);
      }
    });
  });
});
