import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        forbidNonWhitelisted: true,
        transform: true,
      }),
    );
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('welcome to my movie api');
  });
  describe('/movies', () => {
    it('getAll', () => {
      return request(app.getHttpServer()).get('/movies').expect(200).expect([]);
    });

    it('createMovie', () => {
      return request(app.getHttpServer())
        .post('/movies')
        .send({
          title: 'Test',
          year: 2000,
          genres: ['test'],
        })
        .expect(201);
    });

    it('deleteOne', () => {
      return request(app.getHttpServer()).delete('/movies').expect(404);
    });
  });

  describe('/moives/:id', () => {
    it('GET 404', () => {
      return request(app.getHttpServer()).get('/movies/999').expect(404);
    });
    it('GET 200', async () => {
      try {
        const { body } = await request(app.getHttpServer()).get('/movies');
        const item = body.length > 0 ? body[0] : null;
        if (item) {
          return await request(app.getHttpServer())
            .get(`/movies/${item.id}`)
            .expect(200);
        } else {
          return await request(app.getHttpServer()).get('/movies').expect([]);
        }
      } catch (e) {
        console.log(e);
      }
    });
    it('PATCH', async () => {
      try {
        const { body } = await request(app.getHttpServer()).get('/movies');
        const item = body.length > 0 ? body[0] : null;
        if (item) {
          return await request(app.getHttpServer())
            .patch(`/movies/${item.id}`)
            .send({ year: 2025, title: 'updated data' })
            .expect(200);
        } else {
          return await request(app.getHttpServer()).get('/movies').expect([]);
        }
      } catch (e) {
        console.log(e);
      }
    });
    it('DELETE', async () => {
      try {
        const { body } = await request(app.getHttpServer()).get('/movies');
        const item = body.length > 0 ? body[0] : null;
        if (item) {
          return await request(app.getHttpServer())
            .delete(`/movies/${item.id}`)
            .expect(200);
        } else {
          return await request(app.getHttpServer()).get('/movies').expect([]);
        }
      } catch (e) {
        console.log(e);
      }
    });
    it('GET ALL', () => {
      return request(app.getHttpServer()).get('/movies').expect(200).expect([]);
    });
    it('POST 400', () => {
      return request(app.getHttpServer())
        .post('/movies')
        .send({
          title: 'Test',
          year: 2000,
          genres: ['test'],
          other: 'ddd',
        })
        .expect(400);
    });
  });
});
